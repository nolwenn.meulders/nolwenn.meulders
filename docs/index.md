## A propos de moi 

![](images/avatar-photo.jpg) 

Salut ! 

Je m'appel Nolwenn Meulders et j'ai 22 ans. 
J'étudie l'architecture à l'ULB, dans la faculté la Cambre Horta. J'ai décidé de devenir architecte depuis toute petite. A l'époque, je passais mon temps et refaire les plans de ma maison, ma chambre. Je jouais aux sims pour pouvoir créer de nouveaux bâtiments, imaginer la maison de mes rêves. 
Depuis mon style à bien évolué. Si cela vous intéresse, vous pouvez retrouver mon portfolio sur ce [site](https://nolwennmeul.wixsite.com/portfolio), soit tous mes projets effectués lors de mon bachelier. 

## Mon passé et mes activitées 

Je suis Belgo-indonesienne, mais j'ai toujours vécu en Belgique, à Bruxelles. 
J'ai effectué ma scolarité à l'Institut de la Vierge-Fidèle. C'était une école stricte ou l'uniforme nous était imposé. Mais contre toute attente, j'ai adoré mes années au sein de cet établissement. 

Ma passion c'est le scoutisme. J'ai intégré les mouvements de jeunesses depuis mon plus jeune âge. Aujourd'hui, je suis chef scout et c'est à mon tour de transmettre tout ce que j'ai pu apprendre toutes ces années. 


# Objet choisi


**Objet** : _Chaise d'enfant_

**Créateurs** : _Jonathan de Pas, Donato d'Urbino, Paolo Lomazzi, Giorgio de Curso_

**Editeur** : _BBB Bonacina_

![photo](docs/images/Chaises_d_enfants.jpg)

Cet objet en plastique et aux couleurs vives est en réalité une petite chaise pour enfants (49x33x33 cm). Elle est assez légère et peut donc être manipulée par ceux-ci. En plus d'être une assise, cet objet peut être également utilisé comme accessoire de jeux, de construction. Les petites chaises peuvent d'attacher entre elles, se démonter. Leurs utilités sont multiples et leurs combinaisons infinies. 
J'ai décidé de le choisir pour son aspect modulable, combinable. Il me semble intéressant d'étudier un objet sous ses différetnes formes et de découvrir tout ce que celui-ci peut nous apporter. 
